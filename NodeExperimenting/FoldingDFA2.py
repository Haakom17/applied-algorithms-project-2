import itertools




# -------------------- CREATE DFA 2 --------------------

# Linked Nodes for DFA 2
class Node:
    def __init__(self, label):
        self.label = label
        self.state = False
        self.pathTo = {}

    def setLabel(self, label):
        self.label = label

    def setState(self, booly):
        self.state = booly

    def setPath(self, label, value):
        self.pathTo.update({label: value})


# Create Nodes
n1 = Node("1")
n2 = Node("2")
n3 = Node("3")
n4 = Node("4")
n5 = Node("5")
n6 = Node("6")
n7 = Node("7")
n8 = Node("8")


# Initialize Paths
n1.setPath("0", n3)
n1.setPath("1", n5)
n1.setPath("2", n2)
n2.setPath("0", n4)
n2.setPath("1", n6)
n2.setPath("2", n1)
n3.setPath("0", n1)
n3.setPath("1", n7)
n3.setPath("2", n4)
n4.setPath("0", n2)
n4.setPath("1", n8)
n4.setPath("2", n3)
n5.setPath("0", n7)
n5.setPath("1", n1)
n5.setPath("2", n6)
n6.setPath("0", n8)
n6.setPath("1", n2)
n6.setPath("2", n5)
n7.setPath("0", n5)
n7.setPath("1", n3)
n7.setPath("2", n8)
n8.setPath("0", n6)
n8.setPath("1", n4)
n8.setPath("2", n7)


# Set States
n1.setState(True)


# -------------------- GENERATE AND VALIDATE ALL STRINGS FOR DFA 2 --------------------

# Generate all strings up to length "L", from alphabet "alph"
def GenerateAllStrings(alph, L):
    stringList = []

    for i in range(L+1):
        products = itertools.product(alph, repeat=i)
        for element in products:
            temp = ''.join(element)
            stringList.append(temp)

    return stringList


# run "string" through DFA, and return it if the accepting state is "True"
def ValidateString(string):
    currentNode = n1
    stringAsList = list(string)

    # Traverse Nodes
    for i in range(len(string)):
        currentNode = currentNode.pathTo[stringAsList[i]]
        # print(f"Current Node: {currentNode} \n")

    # print(f"Last Node: {currentNode.label}")
    # print(f"Accepting State: {currentNode.state}")
    if currentNode.state:
        return True, string
    else:
        return False, False


# Find all valid strings for the DFA and return list with strings
def FindValidStrings(strings):
    validStrings = []

    # Validate each string in "strings"
    for s in strings:
        check, validString = ValidateString(s)
        if check:
            validStrings.append(validString)

    return validStrings


# -------------------- GENERATE ATPA --------------------

class TreeNode:
    def __init__(self, label):
        self.label = label
        self.state = False
        self.merged = False
        self.parent = None
        self.leftChild = None
        self.centerChild = None
        self.rightChild = None

    def setState(self, booly):
        self.state = booly

    def setMerged(self, booly):
        self.merged = booly

    def setParent(self, parent):
        self.parent = parent

    def setCenterChild(self, child):
        self.centerChild = child

    def setLeftChild(self, child):
        self.leftChild = child

    def setRightChild(self, child):
        self.rightChild = child


def buildPrefixTree_Recursive(depth, currentNode):
    if depth < length:
        global labelIncrement

        if currentNode.leftChild is None:
            # Set left child
            tempNode = TreeNode(labelIncrement)
            print(f"setting left child (Nr. {labelIncrement}) for depth: {depth}")
            labelIncrement += 1
            currentNode.setLeftChild(tempNode)
            tempNode.setParent(currentNode)

            # Jump to left child
            jumpLeft = currentNode.leftChild
            # print(f"Jumping to left child")
            buildPrefixTree_Recursive(depth + 1, jumpLeft)      # Recursive call

        if currentNode.centerChild is None:
            # Set center child
            tempNode = TreeNode(labelIncrement)
            print(f"setting center child (Nr. {labelIncrement}) for depth: {depth}")
            labelIncrement += 1
            currentNode.setCenterChild(tempNode)
            tempNode.setParent(currentNode)

            # Jump to center child
            jumpCenter = currentNode.centerChild
            # print(f"Jumping to center child")
            buildPrefixTree_Recursive(depth + 1, jumpCenter)      # Recursive call

        if currentNode.rightChild is None:
            # Set right child
            tempNode = TreeNode(labelIncrement)
            print(f"setting right child (Nr. {labelIncrement}) for depth: {depth}")
            labelIncrement += 1
            currentNode.rightChild = tempNode
            tempNode.setParent(currentNode)

            # Jump to the right node
            jumpRight = currentNode.rightChild
            # print(f"Jumping to right child")
            buildPrefixTree_Recursive(depth + 1, jumpRight)     # Recursive call

    else:
        pass
        # print(f"maximum depth reached at {depth} \n Returning...")


def SetTreeStates(StartNode, string):
    currentNode = StartNode
    stringAsList = list(string)

    if not string:
        # print(f"Root node state = True")
        currentNode.setState(True)

    else:
        for i in range(len(string) + 1):
            if i == len(string):
                # print(f"Node state = True")
                currentNode.setState(True)

            elif currentNode.leftChild is None or currentNode.rightChild is None:
                print("\nERROR: String too long\n")
                break

            elif stringAsList[i] == "1":
                currentNode = currentNode.leftChild

            elif stringAsList[i] == "2":
                currentNode = currentNode.centerChild

            elif stringAsList[i] == "0":
                currentNode = currentNode.rightChild


# -------------------- MATCHING & MERGING APTA --------------------

# Generate a breadth first list of nodes from "startNode" to the bottom (Limits to prevent infinite loops)
def GenerateBreadthFirstList(starNode, limitCheck, limit):
    breadthFirstList = [starNode]

    # Loop through the list and append children
    for node in breadthFirstList:
        if node.leftChild is not None:
            breadthFirstList.append(node.leftChild)
        if node.centerChild is not None:
            breadthFirstList.append(node.centerChild)
        if node.rightChild is not None:
            breadthFirstList.append(node.rightChild)

        if limitCheck:
            if len(breadthFirstList) > limit:
                break

    return breadthFirstList


# Match each accepting state between "mainTree" and "subTree"
def MatchLabels(mainTree, subTree):
    for i in range(len(subTree)):
        if mainTree[i].state != subTree[i].state:
            return False

    return True


# Set each node in subTree as "merged"
def ChangeMergedStates(subTree):
    for node in subTree:
        node.setMerged(True)


def MergeStates():
    originalNodeList = GenerateBreadthFirstList(root, False, 0)
    uniqueNodes = [root]
    limit = len(originalNodeList)    # Prevents infinite loops

    for i in range(1, len(originalNodeList)):

        if not originalNodeList[i].merged:
            subTree = GenerateBreadthFirstList(originalNodeList[i], True, limit)
            subTreeRoot = subTree[0]
            subTreeParent = subTree[0].parent

            for j in range(len(uniqueNodes)):
                mainTree = GenerateBreadthFirstList(uniqueNodes[j], True, limit)
                mainTreeRoot = mainTree[0]

                # Check if "mainTree" matches "subTree"
                if MatchLabels(mainTree, subTree):

                    if subTreeRoot == subTreeParent.leftChild:
                        subTreeParent.setLeftChild(mainTreeRoot)
                        # Change merge states
                        ChangeMergedStates(subTree)
                        print(f"Node merged")
                        break

                    elif subTreeRoot == subTreeParent.centerChild:
                        subTreeParent.setCenterChild(mainTreeRoot)
                        # Change merge states
                        ChangeMergedStates(subTree)
                        print(f"Node merged")
                        break

                    elif subTreeRoot == subTreeParent.rightChild:
                        subTreeParent.setRightChild(mainTreeRoot)
                        # Change merge states
                        ChangeMergedStates(subTree)
                        print(f"Node merged")
                        break

                    else:
                        print("ERROR: Missmatch between subTree and subTreeParent")
                        return

            if not subTreeRoot.merged:
                print(f"Added unique node")
                uniqueNodes.append(subTreeRoot)

    return uniqueNodes


# -------------------- VALIDATE APTA AFTER MERGE --------------------

# Run one string through the merged APTA and check if the last node is accepting
def ValidateAPTA_SingleString(startNode, string):
    currentNode = startNode
    stringAsList = list(string)

    for i in range(len(string) + 1):
        if i == len(string):
            if currentNode.state:
                return True
            else:
                return False

        if currentNode.leftChild is None or currentNode.rightChild is None:
            print(f"ERROR: String reached dead end\nMerged APTA is incomplete \n")
            return False

        if stringAsList[i] == "1":
            currentNode = currentNode.leftChild

        elif stringAsList[i] == "2":
            currentNode = currentNode.centerChild

        elif stringAsList[i] == "0":
            currentNode = currentNode.rightChild


def ValidateAPTA_AllStrings(startNode, strings):
    for string in strings:
        if not ValidateAPTA_SingleString(startNode, string):
            print(f"ERROR: APTA not valid \n")
            return False

    print(f"Merged APTA is valid \n")
    return True


# -------------------- MANUALLY TRAVERSE APTA --------------------

def TraverseAPTA(startNode):
    currentNode = startNode
    check = True

    while check:
        print(f"Node Label: {currentNode.label}")
        print(f"Accepting state: {currentNode.state}")
        print(f"Left child ('1'): {currentNode.leftChild.label}")
        print(f"Center child ('2'): {currentNode.centerChild.label}")
        print(f"Right child ('0'): {currentNode.rightChild.label} \n")
        traverse = int(input("Enter 0, 1 to traverse: \n"))

        if traverse == 1:
            currentNode = currentNode.leftChild
        elif traverse == 2:
            currentNode = currentNode.centerChild
        elif traverse == 0:
            currentNode = currentNode.rightChild
        else:
            check = False




length = 6
alphabet = ["0", "1", "2"]
labelIncrement = 0
root = TreeNode(labelIncrement)
labelIncrement += 1

print(f"Generating & validating strings for DFA_1 ...")
allStrings = GenerateAllStrings(alphabet, length)
validStrings_DFA1 = FindValidStrings(allStrings)
print(f"Valid strings for DFA_1: \n    {validStrings_DFA1}\n")


print(f"Building prefix tree...")
buildPrefixTree_Recursive(0, root)
print(f"Prefix tree built \n")


print("Setting states...")
for stringDFA1 in validStrings_DFA1:
    SetTreeStates(root, stringDFA1)
print("APTA states set \n")


print("Merging APTA...")
remainingNodes = MergeStates()
print("APTA merged")
print(f"Remaining nodes: {len(remainingNodes)}\n")


print("Validating APTA...")
ValidateAPTA_AllStrings(root, validStrings_DFA1)


print("Traversing tree:")
TraverseAPTA(root)
print(f"Exiting...")















