import itertools



length = 18
alphabet = ["0", "1"]


# Generate all strings up to length "L", from alphabet "alph"
def GenerateAllStrings(alph, L):
    allStrings = []

    for i in range(L):
        products = itertools.product(alph, repeat=i)
        for element in products:
            temp = ''.join(element)
            allStrings.append(temp)

    return allStrings


strings = GenerateAllStrings(alphabet, length)
print(strings)




