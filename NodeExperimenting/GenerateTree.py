

class Node:
    def __init__(self):
        self.state = False
        self.leftChild = None
        self.rightChild = None

    def setState(self, booly):
        self.state = booly

    def setLeftChild(self, child):
        self.leftChild = child

    def setRightChild(self, child):
        self.rightChild = child



def buildPrefixTree_Recursive(depth, currentNode):

    if depth < length:

        if currentNode.leftChild is None:
            # Set left child
            tempNode = Node()
            print(f"setting left child at depth: {depth}")
            currentNode.setLeftChild(tempNode)

            # Jump to left child
            jumpLeft = currentNode.leftChild
            print(f"Jumping to left child")
            buildPrefixTree_Recursive(depth + 1, jumpLeft)      # Recursive call


        if currentNode.rightChild is None:
            # Set right child
            tempNode = Node()
            print(f"setting right child at depth: {depth}")
            currentNode.rightChild = tempNode

            # Jump to the right node
            jumpRight = currentNode.rightChild
            print(f"Jumping to right child")
            buildPrefixTree_Recursive(depth + 1, jumpRight)     # Recursive call

    else:
        print(f"maximum depth reached at {depth} \n Returning...")




length = 2
root = Node()

buildPrefixTree_Recursive(0, root)














