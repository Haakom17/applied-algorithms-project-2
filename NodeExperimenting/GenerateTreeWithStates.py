


class Node:
    def __init__(self):
        self.state = False
        self.leftChild = None
        self.rightChild = None

    def setState(self, booly):
        self.state = booly

    def setLeftChild(self, child):
        self.leftChild = child

    def setRightChild(self, child):
        self.rightChild = child



def buildPrefixTree_Recursive(depth, currentNode):

    if depth < length:

        if currentNode.leftChild is None:
            # Set left child
            tempNode = Node()
            print(f"setting left child for depth: {depth}")
            currentNode.setLeftChild(tempNode)

            # Jump to left child
            jumpLeft = currentNode.leftChild
            print(f"Jumping to left child")
            buildPrefixTree_Recursive(depth + 1, jumpLeft)      # Recursive call


        if currentNode.rightChild is None:
            # Set right child
            tempNode = Node()
            print(f"setting right child for depth: {depth}")
            currentNode.rightChild = tempNode

            # Jump to the right node
            jumpRight = currentNode.rightChild
            print(f"Jumping to right child")
            buildPrefixTree_Recursive(depth + 1, jumpRight)     # Recursive call

    else:
        print(f"maximum depth reached at {depth} \n Returning...")



def SetTreeStates(StartNode, string):

    currentNode = StartNode
    stringAsList = list(string)

    if not string:
        print(f"Root node state = True")
        currentNode.setState(True)

    else:
        for i in range(len(string) + 1):

            if i == len(string):
                print(f"Node state = True")
                currentNode.setState(True)

            elif currentNode.leftChild is None or currentNode.rightChild is None:
                print("\nSTRING TOO LONG \n")
                break

            elif stringAsList[i] == "1":
                currentNode = currentNode.leftChild

            elif stringAsList[i] == "0":
                currentNode = currentNode.rightChild




def printAllStates_Recursive(currentNode):
    print(currentNode.state)

    if currentNode.leftChild is not None:
        jumpLeft = currentNode.leftChild
        printAllStates_Recursive(jumpLeft)

    if currentNode.rightChild is not None:
        jumpRight = currentNode.rightChild
        printAllStates_Recursive(jumpRight)




length = 2      # Max depth of tree (root = 0)
root = Node()

buildPrefixTree_Recursive(0, root)      # Start with depth 0 as root node

print("\nSetting states: ")
SetTreeStates(root, "10")       # Loop this function with all accepted strings

print("\nPrinting states: ")
printAllStates_Recursive(root)






















