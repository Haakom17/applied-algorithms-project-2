
def BuildPrefixTree():
    one = Node('1',True)
    two = Node('2',False)
    three = Node('3',False)
    four = Node('4',False)
    five = Node('5',False)
    six = Node('6',False)
    seven = Node('7',False)
    eight = Node('8',False)

    nodes = [one,two,three,four,five,six,seven,eight]
    for i in range(0,6):
        if(i == 2 or i == 3):
            pass
        else:
            nodes[i].addLink(nodes[i+2])
            nodes[i+2].addLink(nodes[i])

    for i in range(0,8):
        if(i == 0 or i == 2):
            nodes[i].addLink(nodes[i+1])
        if(i == 1 or i == 3):
            nodes[i].addLink(nodes[i+4])
        if(i == 4 or i == 6):
            nodes[i].addLink(nodes[i-4])
        if(i == 5 or i == 7):
            nodes[i].addLink(nodes[i-1])
    
    for i in range(0,8):
        if(i == 0 or i == 2):
            nodes[i].addLink(nodes[i+4])
        if(i == 1 or i == 3):
            nodes[i].addLink(nodes[i-1])
        if(i == 4 or i == 6):
            nodes[i].addLink(nodes[i+1])
        if(i == 5 or i == 7):
            nodes[i].addLink(nodes[i-4])
    
    return nodes

def MatchLabels(i,j,Node):
    pass

def MergeStates(i,j,Node):
    pass

class Node:
    def __init__(self, value, state):
        self.value = value
        self.links = []
        self.state = state
    

    def addLink(self,link):
        self.links.append(link)
    
    def step(self,next):
        return self.links[next]
        

def main():
    nodes = BuildPrefixTree()
    path = "120"
    string = ""
    current = nodes[0]
    print(current.value)
    for i in path:
        print(i)
        current = current.step(int(i))
        string += current.value

    print(string)











if __name__ == '__main__':
    main()