# Applied Algorithms - Project 2

**Grammatical Inference**

Grammatical Inference variously referred to as automata induction, grammar induction, and automatic language 
acquisition, refers to the process of learning of grammars and languages from data. Machine learning of grammars 
finds a variety of applications in syntactic pattern recognition, adaptive intelligent agents, diagnosis, computational 
biology, systems modelling, cybersecurity, prediction, natural language acquisition, data mining and knowledge discovery.


What is the smallest value of L such that the algorithms produce exactly the above DFAs?

	The smalest value of L that will produce both DFAs is 6. 

What happens when the strings are longer than L?

	If the strings are longer than L during creation they will be merged with already existing nodes.
	If the strings are longer than L during traversal, you will loop the DFA.

How is L related to the DFAs? Can you explain why?

	No



