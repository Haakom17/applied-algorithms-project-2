class Node:
    def __init__(self, value):
        self.label = value
        self.one = None
        self.zero = None

    def setOne(self, one):
        self.one = one

    def setZero(self, zero):
        self.zero = zero


class dfa:
    def __init__(self, start):
        self.start = start
        self.accepting = None

    def setAccepting(self, accepting):
        self.accepting = accepting


def checkString(string, dfa):
    stringAsList = string
    currentNode = dfa.start
    for i in range(0, len(stringAsList)):
        if stringAsList[i] == " ":
            stringToReturn = list(stringAsList[0:i])
            if currentNode.label in dfa.accepting:
                return stringToReturn
            else:
                return False
        elif stringAsList[i] == "0":
            currentNode = currentNode.zero
        elif stringAsList[i] == "1":
            currentNode = currentNode.one
    if currentNode.label in dfa.accepting:
        return stringAsList
    else:
        return False

listOfLegalStrings = []

def printAcceptedStrings(dfa, L, alphabet):
    currentList = [" "]*L
    generateList(dfa, L, alphabet, currentList)
    print(listOfLegalStrings)

def generateList(dfa, L, alphabet, stringToCheck, index = 0):
    for letter in alphabet:
        if index == L:
            return
        stringToCheck[index] = letter
        checkedString = checkString(stringToCheck, dfa)
        if checkedString and ''.join(checkedString) not in listOfLegalStrings:
            listOfLegalStrings.append(''.join(checkedString))
        generateList(dfa, L, alphabet, stringToCheck, index + 1)


def main():
    nodeList = []
    for i in range(0, 11):
        newnode = Node(str(i))
        nodeList.append(newnode)

    nodeList[1].setOne(nodeList[7])
    nodeList[2].setOne(nodeList[7])
    nodeList[3].setOne(nodeList[4])
    nodeList[4].setOne(nodeList[1])
    nodeList[5].setOne(nodeList[3])
    nodeList[6].setOne(nodeList[5])
    nodeList[7].setOne(nodeList[10])
    nodeList[8].setOne(nodeList[2])
    nodeList[9].setOne(nodeList[1])
    nodeList[10].setOne(nodeList[4])
    nodeList[1].setZero(nodeList[6])
    nodeList[2].setZero(nodeList[10])
    nodeList[3].setZero(nodeList[4])
    nodeList[4].setZero(nodeList[10])
    nodeList[5].setZero(nodeList[9])
    nodeList[6].setZero(nodeList[6])
    nodeList[7].setZero(nodeList[7])
    nodeList[8].setZero(nodeList[2])
    nodeList[9].setZero(nodeList[9])
    nodeList[10].setZero(nodeList[8])

    dfa1 = dfa(nodeList[1])

    dfa1.setAccepting(["5","6","7","8"])

    printAcceptedStrings(dfa1, 12, ["0","1"," "])


if __name__ == '__main__':
    main()
