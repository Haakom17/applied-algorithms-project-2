import itertools


# Linked Nodes
class Node:
    def __init__(self, label):
        self.label = label
        self.state = False
        self.pathTo = {}

    def setLabel(self, label):
        self.label = label

    def setState(self, booly):
        self.state = booly

    def setPath(self, label, value):
        self.pathTo.update({label: value})


# Create Nodes
n1 = Node("1")
n2 = Node("2")
n3 = Node("3")
n4 = Node("4")
n5 = Node("5")
n6 = Node("6")
n7 = Node("7")
n8 = Node("8")


# Initialize Paths
n1.setPath("0", n3)
n1.setPath("1", n5)
n1.setPath("2", n2)
n2.setPath("0", n4)
n2.setPath("1", n6)
n2.setPath("2", n1)
n3.setPath("0", n1)
n3.setPath("1", n7)
n3.setPath("2", n4)
n4.setPath("0", n2)
n4.setPath("1", n8)
n4.setPath("2", n3)
n5.setPath("0", n7)
n5.setPath("1", n1)
n5.setPath("2", n6)
n6.setPath("0", n8)
n6.setPath("1", n2)
n6.setPath("2", n5)
n7.setPath("0", n5)
n7.setPath("1", n3)
n7.setPath("2", n8)
n8.setPath("0", n6)
n8.setPath("1", n4)
n8.setPath("2", n7)


# Set States
n1.setState(True)


# Generate all strings up to length "L", from alphabet "alph"
def GenerateAllStrings(alph, L):
    stringList = []

    for i in range(L+1):
        products = itertools.product(alph, repeat=i)
        for element in products:
            temp = ''.join(element)
            stringList.append(temp)

    return stringList


# run "string" through DFA, and return path if true
def ValidateString(string):
    currentNode = n1
    stringAsList = list(string)
    labelPath = [currentNode.label]

    # Traverse Nodes
    for i in range(len(string)):
        currentNode = currentNode.pathTo[stringAsList[i]]
        labelPath.append(currentNode.label)
        # print(f"Current Node: {currentNode} \n")

    # print(f"Last Node: {currentNode.label}")
    # print(f"Accepting State: {currentNode.state}")
    if currentNode.state:
        return True, labelPath
    else:
        return False, False


# Find all valid strings for the DFA and return dict with strings and paths
def FindValidStrings(strings):
    validStrings = {}

    # Validate each string in "strings"
    for s in strings:
        check, path = ValidateString(s)
        if check:
            path = str(path)
            validStrings[s] = path

    return validStrings



length = 6
alphabet = ["0", "1", "2"]
allStrings = GenerateAllStrings(alphabet, length)

result = FindValidStrings(allStrings)

for key, val in result.items():
    print(f"{key}: {val}")










